"""
10421
видос : https://www.youtube.com/watch?v=MCfjc_UIP1M&ab_channel=selfedu
читать:https://docs.google.com/document/d/113MSNxUn12QNMdCNrkCFH_gWSxcK4BT-ui7FbXYyR6c/edit
"""

with open("matrix.txt", "r") as file:
    graph = []
    for line in file:
        row = [int(num) for num in line.split()]
        graph.append(row)
    print(graph)


# Функция, реализующая алгоритм Дейкстры
def dijkstra(matrix, start):
        n = len(matrix)
        visited = [False] * n
        dist = [float('inf')] * n
        dist[start] = 0

        for i in range(n):
            min_dist = float('inf')
            min_vertex = -1
            for j in range(n):
                if not visited[j] and dist[j] < min_dist:
                    min_dist = dist[j]
                    min_vertex = j
            if min_vertex == -1:
                break
            visited[min_vertex] = True
            for j in range(n):
                if matrix[min_vertex][j] != 0 and dist[min_vertex] + matrix[min_vertex][j] < dist[j]:
                    dist[j] = dist[min_vertex] + matrix[min_vertex][j]

        result = {}
        for i in range(n):
            result[i] = dist[i]

        return result


# Функция для записи результатов в файл
def write_results(file_name, results):
    with open(file_name, 'w') as f:
        for vertex, distance in dist.items():
            f.write(f'{vertex} : {distance} \n')



 # считываем матрицу смежности из файла

dist = dijkstra(graph,0)  # находим кратчайшие пути из вершины start

write_results('output_file.txt', dist)  # записываем результаты в файл


