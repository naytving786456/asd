"""
30
очень клевый видос: https://www.youtube.com/watch?v=Cbt67mfrF6o&t=578s&ab_channel=%D0%9C%D0%B8%D1%85%D0%B0%D0%B8%D0%BB%D0%A5%D0%BE%D0%B2%D0%B0%D0%B5%D0%B2
так лучше чтобы ты поняла как работают префикс-функции,потому что он попросит тебя составить префикс функцию для строки
"""
def prefix_function(pattern):
    prefix = [0] * len(pattern)
    i = 0
    for j in range(1, len(pattern)):
        while i > 0 and pattern[i] != pattern[j]:
            i = prefix[i - 1]
        if pattern[i] == pattern[j]:
            i += 1
        prefix[j] = i
    return prefix


def kmp(text, pattern):
    prefix = prefix_function(pattern)
    matches = []
    i = 0
    for j in range(len(text)):
        while i > 0 and pattern[i] != text[j]:
            i = prefix[i - 1]
        if pattern[i] == text[j]:
            i += 1
        if i == len(pattern):
            matches.append(j - i + 1)
            i = prefix[i - 1]
    return matches


# считываем текстовый файл в строку
with open('test.txt', 'r') as file:
    text = file.read()

# считываем строку поиска
pattern = input("Введите строку для поиска: ")

# ищем вхождения подстроки pattern в строку text, используя алгоритм Кнута-Морриса-Пратта
matches = kmp(text, pattern)

# выводим найденные индексы вхождений
print(matches)