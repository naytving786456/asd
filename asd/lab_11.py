"""
тут инфы мало , но можешь почитать в презах тенебекова(32-34) https://docs.yandex.ru/docs/view?url=ya-disk-public%3A%2F%2FesUEQL61jtnqmOXqE1h4E4bUQ9wnGLyg5KJNaO0XUWhmANNT7RpTiQ34NGs%2Bs28Jq%2FJ6bpmRyOJonT3VoXnDag%3D%3D%3A%2F%D0%9F%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D0%B8%2F23-24-%D0%9F%D0%BE%D0%B8%D1%81%D0%BA%20%D1%86%D0%B5%D0%BF%D0%BE%D1%87%D0%B5%D0%BA%20%D1%81%D0%B8%D0%BC%D0%B2%D0%BE%D0%BB%D0%BE%D0%B2.pptx&name=23-24-%D0%9F%D0%BE%D0%B8%D1%81%D0%BA%20%D1%86%D0%B5%D0%BF%D0%BE%D1%87%D0%B5%D0%BA%20%D1%81%D0%B8%D0%BC%D0%B2%D0%BE%D0%BB%D0%BE%D0%B2.pptx
или я сам тебе потом постараюсь объяснить
"""
def finite_automaton_matcher(txt, pat):
    m = len(pat)
    n = len(txt)
    tf = compute_transition_function(pat)

    q = 0
    matches = []
    for i in range(n):
        while q > 0 and pat[q] != txt[i]:
            q = tf[q-1]
        if pat[q] == txt[i]:
            q += 1
        if q == m:
            matches.append(i-m+1)
            q = tf[q-1]
    return matches

def compute_transition_function(pat):
    m = len(pat)
    tf = [0] * m
    i, j = 1, 0
    while i < m:
        if pat[i] == pat[j]:
            tf[i] = j + 1
            i += 1
            j += 1
        elif j > 0:
            j = tf[j-1]
        else:
            tf[i] = 0
            i += 1
    return tf

# Считываем файл и задаём образец
with open("test.txt", "r") as f:
    txt = f.read().strip()
pat = input("Введите строку для поиска: ")

# Ищем образец в строке с помощью конечного автомата
matches = finite_automaton_matcher(txt, pat)

# Выводим индексы найденных совпадений и матрицу соответствия
print(matches)



