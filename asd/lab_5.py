"""
17115
тут самое интересное , данный алгоритм находим компоненты сильной связности через алгоритм Косарайю
о нем тут (20 слайд) https://docs.yandex.ru/docs/view?url=ya-disk-public%3A%2F%2FesUEQL61jtnqmOXqE1h4E4bUQ9wnGLyg5KJNaO0XUWhmANNT7RpTiQ34NGs%2Bs28Jq%2FJ6bpmRyOJonT3VoXnDag%3D%3D%3A%2F%D0%9F%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D0%B8%2F15-%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0%20%D0%B2%20%D0%B3%D0%BB%D1%83%D0%B1%D0%B8%D0%BD%D1%83%20%D0%B8%20%D1%88%D0%B8%D1%80%D0%B8%D0%BD%D1%83%20%D0%B2%20%D0%B3%D1%80%D0%B0%D1%84%D0%B5.pptx&name=15-%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0%20%D0%B2%20%D0%B3%D0%BB%D1%83%D0%B1%D0%B8%D0%BD%D1%83%20%D0%B8%20%D1%88%D0%B8%D1%80%D0%B8%D0%BD%D1%83%20%D0%B2%20%D0%B3%D1%80%D0%B0%D1%84%D0%B5.pptx&nosw=1
или тут https://habr.com/ru/articles/537290/
"""
# Функция для чтения матрицы смежности из файла
def read_adjacency_matrix(file_path):
    adjacency_matrix = []
    with open(file_path, 'r') as f:
        for line in f:
            row = list(map(int, line.strip().split()))
            adjacency_matrix.append(row)
    return adjacency_matrix

# Функция для записи результата в файл
def write_result(result, file_path):
    with open(file_path, 'w') as f:
        for vertex, components in result.items():
            f.write(str(vertex) + ':' + str(components) + '\n')

# Функция поиска количества и состава компонент сильной связности
def dfs_strongly_connected_components(adjacency_matrix):
    n = len(adjacency_matrix)
    reverse_adjacency_matrix = [[adjacency_matrix[j][i] for j in range(n)] for i in range(n)]
    visited = [False] * n
    finish_time_stack = []

    def dfs_reverse(vertex):
        visited[vertex] = True

        for neighbor, weight in enumerate(reverse_adjacency_matrix[vertex]):
            if weight != 0 and not visited[neighbor]:
                dfs_reverse(neighbor)

        finish_time_stack.append(vertex)

    def dfs(vertex, components, component_number):
        visited[vertex] = True
        components[component_number].append(vertex)

        for neighbor, weight in enumerate(adjacency_matrix[vertex]):
            if weight != 0 and not visited[neighbor]:
                dfs(neighbor, components, component_number)

    for start_vertex in range(n):
        if not visited[start_vertex]:
            dfs_reverse(start_vertex)

    visited = [False] * n
    scc = {}
    component_number = 0

    while finish_time_stack:
        vertex = finish_time_stack.pop()
        if not visited[vertex]:
            component_number += 1
            scc[component_number] = []
            dfs(vertex, scc, component_number)

    return {v: k for k, c in scc.items() for v in c}

adjacency_matrix = read_adjacency_matrix('graph.txt')
components = dfs_strongly_connected_components(adjacency_matrix)
write_result(components, 'result.txt')