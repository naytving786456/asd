def boyer_moore_search(text, pattern):
    n, m = len(text), len(pattern)

    if m == 0:
        return []

    # Создаем словарь смещений, где ключ - символ, значение - смещение
    skip = {c: m - i - 1 for i, c in enumerate(pattern[:-1])}
    skip[pattern[-1]] = m if m < 2 else 1

    # Используем указатели i, j для сканирования текста и образца соответственно
    i = m - 1
    result = []

    while i < n:
        j = m - 1
        while text[i] == pattern[j]:
            if j == 0:
                result.append(i)
                break
            i -= 1
            j -= 1
        i += skip.get(text[i], m)

    return result

with open('test.txt', 'r') as f:
        text = f.read().rstrip()

pattern = input("Введите строку поиска: ")

result = boyer_moore_search(text, pattern)

if len(result) == 0:
        print("Совпадений не найдено")
else:
    print("Совпадения найдены по следующим индексам: ", result)
