"""
Подробнее про алгоритм можешь почитать тут https://habr.com/ru/articles/144921/
Советую разобраться как он устроен, как осущствляется поворот и прочие приколы
171626
"""

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def graham_scan(points):
    def cross(o, a, b):
        return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x)

    hull = []
    for p in sorted(points, key=lambda p: p.x):
        while len(hull) >= 2 and cross(hull[-2], hull[-1], p) < 0:
            hull.pop()
        hull.append(p)
    for p in sorted(points, key=lambda p: p.x, reverse=True):
        while len(hull) >= 2 and cross(hull[-2], hull[-1], p) < 0:
            hull.pop()
        hull.append(p)
    return hull
    print(hull)

n = int(input("Введите число точек: "))
points = []
for i in range(n):
    x, y = map(int, input(f"Введите координаты точки {i+1}: ").split())
    points.append(Point(x, y))

# Вызываем функцию для определения выпуклой оболочки и выводим результат
convex_hull = graham_scan(points)
if len(convex_hull) <= 2:
    print("Выпуклой оболочки не существует")
else:
    print("Выпуклая оболочка")
    for point in convex_hull:
        print(f'({point.x};{point.y})')