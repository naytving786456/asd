"""
61013
видос:https://www.youtube.com/watch?v=_SLQMpLzcUE&t=247s&ab_channel=selfedu
читать:https://docs.google.com/document/d/1WdemPF8dfd-rHCqNCV3bvuhhPhPm-CgDAcvm-B5KUx4/edit
"""
import sys


# Функция чтения матрицы смежности графа из файла
def read_graph(file_name):
    graph = []
    with open(file_name, 'r') as f:
        for line in f:
            graph.append([int(x) for x in line.split()])
    return graph


# Алгоритм Прима
def prim(graph):
    n = len(graph)
    # Инициализация массивов
    selected_vertex = [0] * n
    key = [sys.maxsize] * n
    parent = [-1] * n
    # Случайным образом выбираем начальную вершину
    key[0] = 0

    for i in range(n):
        # Находим вершину с минимальным весом
        min_vertex = -1
        for j in range(n):
            if (not selected_vertex[j]) and (min_vertex == -1 or key[j] < key[min_vertex]):
                min_vertex = j
        selected_vertex[min_vertex] = 1

        # Обновляем ключи и родительские вершины
        for v in range(n):
            if graph[min_vertex][v] != 0 and graph[min_vertex][v] < key[v] and not selected_vertex[v]:
                key[v] = graph[min_vertex][v]
                parent[v] = min_vertex

    # Создаем словарь для хранения вершин и их весов
    vertex_weights = {}
    for i in range(1, n):
        vertex_weights[str(parent[i]) + "-" + str(i)] = graph[parent[i]][i]

    return vertex_weights  # Возвращает словарь вершина-вершина : вес


# Чтение матрицы смежности из файла и запуск алгоритма Прима
graph = read_graph('matrix.txt')
result = prim(graph)

# Вывод результатов на экран
with open("out_prim.txt",'w') as f:
    for k, v in result.items():
        f.write(f'{k} : {v}\n')
f.close()