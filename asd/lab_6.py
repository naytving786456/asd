"""
191216
посмотри видос:https://www.youtube.com/watch?v=KDKACf8tcnM
и почитай тут  https://docs.google.com/document/d/1kreNNjqPG-hSO1zcOyM0V6yqLLEOnOjNNFfbXa_VhPo/edit
"""
# Читаем матрицу смежности из файла
with open("matrix.txt", "r") as file:
    graph = []
    for line in file:
        row = [int(num) for num in line.split()]
        graph.append(row)

# Инициализируем пустой список ребер и номера вершин
edges = []
num_vertices = len(graph)

# Вспомогательный метод для нахождения родителя вершины
def parent(parents, vertex):
    if parents[vertex] == vertex:
        return vertex
    return parent(parents, parents[vertex])

# Инициализируем родителей обеих вершин и используем цикл для заполнения списка ребер
for i in range(num_vertices):
    for j in range(i + 1, num_vertices):
        if graph[i][j]:
            edges.append((i, j, graph[i][j]))

edges.sort(key=lambda x: x[2]) # Сортируем список ребер в порядке возрастания взвешенности

# Используем методы слияния и проверки циклов для определения минимального остовного дерева
parents = [i for i in range(num_vertices)]
mst = []
for edge in edges:
    parent1 = parent(parents, edge[0])
    parent2 = parent(parents, edge[1])
    if parent1 != parent2:
        mst.append(edge)
        parents[parent1] = parent2

# Выводим ребра минимального остовного дерева
with open("out_kraskal.txt",'w') as f:
    for edge in mst:
        f.write(f'{edge[0]} - {edge[1]} : {edge[2]}\n')
f.close()

