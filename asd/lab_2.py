"""
Про поиск в ширину можно почитать тут : https://docs.google.com/document/d/1sSELYVOD5VZs-vgo0AHxrbSJMxZE0uj2PtbGDko52Do/edit
Миша собрал для нас все.
131017
"""
from queue import Queue

# Функция для чтения матрицы смежности из файла
def read_adjacency_matrix(file_path):
    adjacency_matrix = []
    with open(file_path, 'r') as f:
        for line in f:
            row = list(map(int, line.strip().split()))
            adjacency_matrix.append(row)
    return adjacency_matrix

# Функция для записи результата в файл
def write_result(result, file_path):
    with open(file_path, 'w') as f:
        start_vertex = result[0]
        f.write(str(start_vertex) + '\n')
        for vertex, distance in result[1:]:
            f.write(str(vertex) + ':' + str(distance) + '\n')

# Функция поиска кратчайшего пути из заданной вершины до всех остальных
def bfs_shortest_path(adjacency_matrix, start):
    n = len(adjacency_matrix)
    distances = [float('inf')] * n
    distances[start] = 0

    visited = [False] * n
    visited[start] = True

    queue = Queue()
    queue.put(start)

    while not queue.empty():
        current_vertex = queue.get()

        for neighbor, weight in enumerate(adjacency_matrix[current_vertex]):
            if weight == 0:
                continue
            if not visited[neighbor]:
                visited[neighbor] = True
                distances[neighbor] = distances[current_vertex] + weight
                queue.put(neighbor)
            elif distances[neighbor] > distances[current_vertex] + weight:
                distances[neighbor] = distances[current_vertex] + weight

    return [(i, d) for i,d in enumerate(distances)]



adjacency_matrix = read_adjacency_matrix('graph.txt')
start_vertex = 0
result = bfs_shortest_path(adjacency_matrix, start_vertex)
write_result(result, 'result.txt')