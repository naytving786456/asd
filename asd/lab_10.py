"""
133320
читать: https://docs.google.com/document/d/1PAjw77CdL6ch3VjbDn4Rqg5ajiC0X6fr6NVlhBL0R48/edit
или
Алгоритм нахождения эйлерова цикла:
1. Выбрать произвольную вершину графа и добавить ее в путь.
2. Найти любую смежную вершину с выбранной и добавить ее в путь.
3. Продолжать этот процесс, пока не будет нарушено условие, что любая вершина имеет четную степень, и все ребра графа пройдены (цикл посетил все ребра).
4. Если сталкиваемся с вершиной, которая имеет нечетную степень, сохранить текущий путь и начать новый, используя эту вершину в качестве начальной.
5. Продолжать пока все вершины не будут удовлетворять условию четности степеней.
6. Объединить все сохраненные пути в один путь.
"""
def find_euler_path(adj_matrix):
    n = len(adj_matrix)
    stack = [0]
    path = []
    while stack:
        u = stack[-1]
        v = None
        for i in range(n):
            if adj_matrix[u][i]:
                v = i
                break
        if v is not None:
            stack.append(v)
            adj_matrix[u][v] = 0
            adj_matrix[v][u] = 0
        else:
            path.append(stack.pop())
    if sum(sum(row) for row in adj_matrix) == 0:
        return path[::-1]
    else:
        return None

# чтение матрицы смежности из файла
with open('graph.txt', 'r') as f:
    adj_matrix = [[int(x) for x in line.split()] for line in f]

# поиск эйлерова пути
path = find_euler_path(adj_matrix)

# вывод результата в файл
with open('cycle.txt', 'w') as f:
    if path is not None and path[0]==path[-1]:
        f.write(' '.join(str(x) for x in path))
    else:
        f.write('Graph is not eulerian')


