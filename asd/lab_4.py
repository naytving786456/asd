"""
тут про поиск в глубину: https://docs.google.com/document/d/1biEBlfX53hDde-FLyImPLFGb04cMRbXVmuywo9I_WQ4/edit
26114
"""
# Функция для чтения матрицы смежности из файла
def read_adjacency_matrix(file_path):
    adjacency_matrix = []
    with open(file_path, 'r') as f:
        for line in f:
            row = list(map(int, line.strip().split()))
            adjacency_matrix.append(row)
    return adjacency_matrix

# Функция для записи результата в файл
def write_result(result, file_path):
    with open(file_path, 'w') as f:
        for vertex, components in result.items():
            f.write(str(vertex) + ':' + str(components) + '\n')

# Функция поиска количества и состава компонент связности
def dfs_connected_components(adjacency_matrix):
    n = len(adjacency_matrix)
    visited = [False] * n
    components = {}
    component_number = 0

    def explore(vertex):
        nonlocal component_number
        visited[vertex] = True
        components[component_number].append(vertex)

        for neighbor, weight in enumerate(adjacency_matrix[vertex]):
            if isinstance(weight, int) and weight != 0 and not visited[neighbor]:
                explore(neighbor)

    for start_vertex in range(n):
        if not visited[start_vertex]:
            component_number += 1
            components[component_number] = []
            explore(start_vertex)

    return {v: k for k, c in components.items() for v in c}


# Пример использования
adjacency_matrix = read_adjacency_matrix('graph.txt')
components = dfs_connected_components(adjacency_matrix)
write_result(components, 'result.txt')