"""
102030
Из през Тенебекова можно почитать про компоненты связности графа(16 слайд)
https://docs.yandex.ru/docs/view?url=ya-disk-public%3A%2F%2FesUEQL61jtnqmOXqE1h4E4bUQ9wnGLyg5KJNaO0XUWhmANNT7RpTiQ34NGs%2Bs28Jq%2FJ6bpmRyOJonT3VoXnDag%3D%3D%3A%2F%D0%9F%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D0%B8%2F15-%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0%20%D0%B2%20%D0%B3%D0%BB%D1%83%D0%B1%D0%B8%D0%BD%D1%83%20%D0%B8%20%D1%88%D0%B8%D1%80%D0%B8%D0%BD%D1%83%20%D0%B2%20%D0%B3%D1%80%D0%B0%D1%84%D0%B5.pptx&name=15-%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B%20%D0%BF%D0%BE%D0%B8%D1%81%D0%BA%D0%B0%20%D0%B2%20%D0%B3%D0%BB%D1%83%D0%B1%D0%B8%D0%BD%D1%83%20%D0%B8%20%D1%88%D0%B8%D1%80%D0%B8%D0%BD%D1%83%20%D0%B2%20%D0%B3%D1%80%D0%B0%D1%84%D0%B5.pptx&nosw=1
"""
from queue import Queue

# Функция для чтения матрицы смежности из файла
def read_adjacency_matrix(file_path):
    adjacency_matrix = []
    with open(file_path, 'r') as f:
        for line in f:
            row = list(map(int, line.strip().split()))
            adjacency_matrix.append(row)
    return adjacency_matrix

# Функция для записи результата в файл
def write_result(result, file_path):
    with open(file_path, 'w') as f:
        for vertex, components in result.items():
            f.write(str(vertex) + ':' + str(components) + '\n')

# Функция поиска количества и состава компонент связности
def bfs_connected_components(adjacency_matrix):
    n = len(adjacency_matrix)
    visited = [False] * n
    components = {}
    component_number = 0

    for start_vertex in range(n):
        if not visited[start_vertex]:
            component_number += 1
            queue = Queue()
            visited[start_vertex] = True
            components[component_number] = []
            queue.put(start_vertex)

            while not queue.empty():
                current_vertex = queue.get()
                components[component_number].append(current_vertex)

                for neighbor, weight in enumerate(adjacency_matrix[current_vertex]):
                    if isinstance(weight, int) and weight == 0:
                        continue
                    elif not visited[neighbor]:
                        visited[neighbor] = True
                        queue.put(neighbor)

    return {v: k for k, c in components.items() for v in c}


# Пример использования
adjacency_matrix = read_adjacency_matrix('graph.txt')
components = bfs_connected_components(adjacency_matrix)
write_result(components, 'result.txt')